# pdizzle
# 12/13/2002
# 4558 : Network Traffic Analysis

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def append(df, data):
    #############
    # Simple function to append a list to df as a new row
    #############
    df.loc[len(df.index)] = data
    
def run_tshark(filename, name):
    #############
    # Runs multiple tshark commands to pull necessary data for df
    #  - Returns data in list form
    #############
        
    # Add name of pcap with tool as first col in df
    data = [name]
    
    # Files to write command output to
    write_files = ["output/txt/total_packets.txt", \
                   "output/txt/successful_attempts.txt", \
                   "output/txt/unsuccessful_attempts.txt", \
                   "output/txt/describe_packets.txt", \
                    "output/txt/unique_describe_packets.txt"]
    
    # Get Total # of Packets in each pcap
    cmd = "tshark -r " + filename + " | wc -l > output/txt/total_packets.txt"
    os.system(cmd)

    # Get Total # of successful attempts from each pcap
    tshark_filter_200 = '"rtsp.response contains "200""'
    cmd = "tshark -r " + filename + " -Y " + tshark_filter_200 + " | wc -l > output/txt/successful_attempts.txt"
    os.system(cmd)

    # Get Total # of unsuccessful attempts from each pcap
    tshark_filter_unsuccessful = '"rtsp.response contains "401""'
    cmd = "tshark -r " + filename + " -Y " + tshark_filter_unsuccessful + " | wc -l > output/txt/unsuccessful_attempts.txt"
    os.system(cmd)
    
    # Get Total # of Describe packets sent from each pcap
    tshark_filter_unsuccessful = '"rtsp.request"'
    cmd = "tshark -r " + filename + " -Y " + tshark_filter_unsuccessful + " | wc -l > output/txt/describe_packets.txt"
    os.system(cmd)
    
    # Get Total # of "Unique" describe packets sent from each pcap
    cmd = 'tshark -r ' + filename +  ' -Y rtsp.request | grep -oP "DESCRIBE\K.*" | cut -d " " -f 2 | uniq | wc -l > output/txt/unique_describe_packets.txt'
    os.system(cmd)
    
    # Get all time values from each pcap
    cmd = 'tshark -r ' + filename + " -Y rtsp.request | awk '{$1=$1};1' | cut -d \" \" -f 2 > output/txt/times.txt"
    os.system(cmd)
    
    # Write the one number from each file (metric recorded per pcap) to data list (which will later be added to df row by row)
    for x in write_files:
        with open(x, "r") as f:
            for line in f:
                data.append(int(line))
    
    # Total Attempts is calculated by adding Successful_Attempts + Unsuccessful Attempts (because some pcaps dont have syn packets)
    total_attempts = data[2] + data[3]
    data.append(total_attempts) 
    
    # Take all times recorded in times.txt and place in list times (starting with time 0)
    times = [0]
    with open("output/txt/times.txt") as f:
        for time in f:
            times.append(float(time.strip()))
    
    # Subtract each time from previous time to get "Time between attacks"
    between = []
    index = 0
    for x in range(len(times)-1):
        between_time = times[index+1] - times[index]
        between.append(between_time)
        index += 1             
    
    #Take the average of time between attempts and add to df    
    average_time_between_attempts = round(sum(between) / len(between), 4)
    data.append(average_time_between_attempts)
    
    # Return new row to be added to df to main()
    return data

    
def graph_metric_average(df, metric):
    #############
    # Creates a graph from averages of metric passed in
    # - Saves graph as "output/<metric>_average.pdf"
    #############
    
    # Ensure plot is clear before creating new plot
    plt.clf()
    
    # Arrange new plot to have groupings of 2 bars ('Amcrest', 'Simcam')
    x = np.arange(2)
    
    # Save metrics into a list
    metric_list = df[metric]
    
    # Divide metrics into seperate lists based on IOT device and tool and take the AVERAGE (hardcoded value of len, could change later)
    average_amcrest_hydra = sum([metric_list[0], metric_list[1], metric_list[2]]) / 3
    average_amcrest_nmap = sum([metric_list[6], metric_list[7], metric_list[8]]) / 3
    average_simcam_hydra = sum([metric_list[3], metric_list[4], metric_list[5]]) / 3
    average_simcam_nmap = sum([metric_list[9], metric_list[10]]) / 2
    
    # Place metrics in list based on iot device
    amcrest = [average_amcrest_hydra, average_amcrest_nmap]
    simcam = [average_simcam_hydra, average_simcam_nmap]

    # Width of bar
    width = 0.2
    
    # Plot data (first bit says where to place each bar in respect to center)
    plt.bar(x-0.1, amcrest, width, color='cyan')  
    plt.bar(x+0.1, simcam, width, color='orange')
    
    # More plot configurations
    plt.xticks(x+0.1, ["Hydra", "Nmap"])
    plt.ylabel(metric)
    plt.legend(['Amcrest', 'SimCam'])
    plt.savefig("output/" + metric + "_average.pdf")
    


def graph_metric(df, metric):
    #############
    # Creates a graph from metric passed in. Separate bar for each pcap found
    # - Saves graph as "output/<metric>.pdf"
    #############
    
    # Ensure plot is clear before creating new plot
    plt.clf()
    
    # Create bar plot from metric passed to function
    ax = df.plot(x='Filename', y=metric, kind="bar")    
    fig = ax.get_figure()
    fig.tight_layout()

    # Save figure to file
    fig.savefig("output/" + metric + ".pdf")
    
                    
def main():
    # Create dataframe      
    df = pd.DataFrame(columns = ['Filename', 'Total Packets', 'Successful Attempts','Unsuccessful Attempts', 'Describe Packets', 'Unique Describe Packets', 'Total Attempts', 'Average Time Between Attempts (seconds)'])
    
    # Will store names of pcaps from all sub-directories
    filenames = []

    # Location of pcaps (starting point)    
    cwd = "data/"
    
    # Better labels (than filenames) for graphing
    ax_labels = ['Amcrest/Hydra 1', 'Amcrest/Hydra 2', 'Amcrest/Hydra 3', \
                'SimCam/Hydra 1', 'Simcam/Hydra 2', 'Simcam/Hydra 3', \
                 'Amcrest/Nmap 1', 'Amcrest/Nmap 2', 'Amcrest/Nmap 3', \
                'SimCam/Nmap 1', 'SimCam/Nmap 2']
    # Used to iterate through names created above
    name_index = 0
    
    # Walk directory data to move through pcaps
    for root, dirs, files in os.walk(cwd, topdown=True):
        for x in files:
            # Save name of file to filename list
            filenames.append(x)
            # Skip over files that might be in directory that aren't pcaps
            if ".pcap" in x:
                filename = '"' + root + "/" + x + '"'
                name = ax_labels[name_index]
                
                # Run tshark commands on each pcap found in sub-directories
                data = run_tshark(filename, name)
                
                # This is used to watch as program executes
                print(data)
                
                # Append new row to df
                append(df, data)
                
                # Increment name index from ax_labels
                name_index += 1
    
    # Print final df after execution
    print(df)
    
    # Metrics to create graphs from (most of columns in df)
    metrics = ['Total Packets', 'Successful Attempts','Unsuccessful Attempts', 'Describe Packets', 'Unique Describe Packets', 'Total Attempts', 'Average Time Between Attempts (seconds)']
    for metric in metrics:
        graph_metric(df, metric)
        graph_metric_average(df, metric)
        
    # Save dataframe to csv
    df.to_csv("output/stats_output.csv", index=False)

    
main()
    
