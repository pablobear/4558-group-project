Number of URL guess attempts during successful runs

Total time spent on the brute force attack for each successful attack

Total time spent for each failed attack

It would also be useful to know how many attempts are tried during failed attacks using both Hydra/Nmap, but that would be a statistic to put in the analysis slide and not a graph (since this is just the size of the URL dictionaries)