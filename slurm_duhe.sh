#!/bin/bash
#SBATCH --job-name=traffic_df
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --time=10-00:00:00
                                                           
#SBATCH -o runs/traffic_df.%j.out
#SBATCH --partition=barton
#SBATCH --mem=64000
#SBATCH --gres=gpu:1
#SBATCH --mail-type=END              
#SBATCH --mail-user=paul.duhe@nps.edu
 
#. /etc/profile
#/share/spack/gcc-7.2.0/miniconda3-4.5.12-gkh/condabin/conda activate tfp-2.6
 
python3 iterator.py
