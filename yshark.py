import pyshark
import os
import string
import asyncio

def get_files():
    # File Structure:
    #   Hydra :
    #       Amcrest - 3 pcaps 
    #           "AmcrestCamBruteForce_#.pca"
    #       SimCam - 3 pcaps
    #           "SimcamBruteForce_#.pcap"
    #   Nmap :   
    #       Amcrest - 3 pcaps
    #           "AmcrestCamRTSP_#.pcap"
    #       SimCam - 2 pcaps
    #           "SIMCAMRTSP_#.pcap"

    cwd = os.getcwd()
    hydra_amcrest = cwd + "/data/hydra/Amcrest Camera/AmcrestCamBruteForce_"
    hydra_simcam = cwd + "/data/hydra/SimCam/SimcamBruteForce_"
    nmap_amcrest = cwd + "/data/nmap/Amcrest Camera/AmcrestCamRTSP_"
    nmap_simcam = cwd + "/data/nmap/SimCam/SIMCAMRTSP_"

    data_dict = {hydra_amcrest:3, hydra_simcam:3, nmap_amcrest: 3, nmap_simcam: 2}
    for pcap, number in data_dict.items():
        filenum = 1
        for x in range(number):
            print(pcap + str(x) + ".pcap")
            filename = pcap + str(filenum) + ".pcap"
            #print("Parsing " + filename + str(filenum) + ".pcap")
            cap = pyshark.FileCapture(filename)

            return cap

def everything_else():
    rtsp_count = 0
    rtsp_list = []

    packet_count = 0
    cap = get_files()
    for x in cap:
        packet_count += 1

    print("Packets in cap:", packet_count)

    for x in range(packet_count):
        packet = cap[x]
        try:
            a = packet.rtsp.field_names
            rtsp_list.append(x)
            rtsp_count += 1
        except:
            pass
    print("RTSP packets:", rtsp_count)

    bad_brute = 0
    attempts = 0
    for x in rtsp_list:
        try:
            request = cap[x].rtsp.request
            attempts += 1
        except:
            response = str(cap[x].rtsp.response)
            if "401" in response:
                bad_brute += 1
    print("Attempts:", attempts)
    print("Unsuccessful 401 replies:", bad_brute)




                
everything_else()