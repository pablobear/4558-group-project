import dpkt 
import pandas as pd
import socket
import matplotlib.pyplot as plt
from datetime import datetime
import sys
import numpy as np
import os

def create_df():
    df = pd.DataFrame(columns = ['PCAP Name', 'Exploit Tool', '# of Packets', '# of ARP Packets', 'Unique IPs',\
        'total_bits', 'average_traffic_rate', 'total_ipv4_packets', 'unique_ips', 'unique_src_ports',\
            'unique_dst_ports'])
    return df

def get_stats(data, filename):
    stats_df = pd.DataFrame(data, columns = ['timestamp', 'ip_size', 'src_ip', 'dst_ip', 'src_port', 'dst_port', 'protocol', 'version'])

    # Total Bits
    total_bits = stats_df['ip_size'].sum()

    # Average traffic rate
    first_ts = (stats_df.timestamp[0])
    last_ts = (stats_df.timestamp[len(stats_df.index)-1])
    average_traffic_rate = (total_bits*8)/(last_ts - first_ts)
        
    # Number of IPV4 packets
    total_ipv4_packets = sum(stats_df['version'] == 4)
    
    # Number of unique IPs
    unique_ips = len(stats_df.dst_ip.unique())
    
    # Number of unique Ports
    unique_src_ports = len(stats_df.src_port.unique())
    unique_dst_ports = len(stats_df.dst_port.unique())

#    print(filename)
#    print(stats_df.protocol.value_counts())
#    print(stats_df.src_port.value_counts())

    print(filename)
    print(stats_df.src_port.value_counts())    
    
    return total_bits, average_traffic_rate, total_ipv4_packets, unique_ips, unique_src_ports, unique_dst_ports


def append_to_df(df, data):
    df.loc[len(df.index)] = data


def parse_pcap(filename):
    fd = open(filename, 'rb')
    pcap = dpkt.pcap.Reader(fd)
    packet = 0
    arp_packet_count = 0
    data = []

    tslist = []
    ipsrclist = []
    ipdstlist = []
    srcprtlist = []
    dstprtlist = []
    protolist = []
    verlist = []
    for ts, buf in pcap:
        packet += 1
        eth = dpkt.ethernet.Ethernet(buf)
        ip = eth.data
        try: 
            ip_src = socket.inet_ntoa(ip.src)
            ip_dst = socket.inet_ntoa(ip.dst)
            
            if ip.p == 6:
                ip_proto = 'TCP'
            elif ip.p == 17:
                ip_proto = 'UDP'
            elif ip.p == 1:
                ip_proto = 'ICMP'
            elif ip.p == 4:
                ip_proto = 'IP-in-IP'
            elif ip.p == 47:
                ip_proto = 'GRE'
            elif ip.p == 132:
                ip_proto = 'SCTP'
            else: ip_proto = ip.p

            try: 
                srcport = ip.data.sport
                dstport = ip.data.dport
            except:
                srcport = 0
                dstport = 0
            data.append([ts, len(ip), ip_src, ip_dst, srcport, dstport, ip_proto, ip.v])
        except:
            # This is an ARP packet
            arp_packet_count += 1

    total_bits, average_traffic_rate, total_ipv4_packets, \
    unique_ips, unique_src_ports, unique_dst_ports \
    = get_stats(data, filename)

    #print("Packets in pcap:", packet)
    #print("ARP packets in pcap:", arp_packet_count)
    return packet, arp_packet_count, total_bits, average_traffic_rate, \
        total_ipv4_packets, unique_ips, unique_src_ports, unique_dst_ports
        
    # File Structure:
#   Hydra :
#       Amcrest - 3 pcaps 
#           "AmcrestCamBruteForce_#.pca"
#       SimCam - 3 pcaps
#           "SimcamBruteForce_#.pcap"
#   Nmap :   
#       Amcrest - 3 pcaps
#           "AmcrestCamRTSP_#.pcap"
#       SimCam - 2 pcaps
#           "SIMCAMRTSP_#.pcap"

cwd = os.getcwd()
hydra_amcrest = cwd + "/data/hydra/Amcrest Camera/AmcrestCamBruteForce_"
hydra_simcam = cwd + "/data/hydra/SimCam/SimcamBruteForce_"
nmap_amcrest = cwd + "/data/nmap/Amcrest Camera/AmcrestCamRTSP_"
nmap_simcam = cwd + "/data/nmap/SimCam/SIMCAMRTSP_"

data_dict = {hydra_amcrest:3, hydra_simcam:3, nmap_amcrest: 3, nmap_simcam: 2}
df = create_df()
for filename, number in data_dict.items():
    filenum = 1
    for x in range(number):
        basename = os.path.basename(filename) + str(filenum) + ".pcap"
        #print("Parsing " + filename + str(filenum) + ".pcap")
        
        packet_count, arp_count, total_bits, average_traffic_rate, \
        total_ipv4_packets, unique_ips, unique_src_ports, unique_dst_ports \
            = parse_pcap(filename + str(filenum) + ".pcap")
      
        
        if ('hydra' in filename):
            exploit_tool = 'hydra'
        else:
            exploit_tool = 'nmap'
        new_row = [basename, exploit_tool, packet_count, arp_count, unique_ips, total_bits, average_traffic_rate, \
        total_ipv4_packets, unique_ips, unique_src_ports, unique_dst_ports]
        append_to_df(df, new_row)
        filenum += 1

#fd = open(hydra_amcrest,"rb")
#print("opening", hydra_amcrest)
#pcap = dpkt.pcap.Reader(fd)


def test():
    cwd = os.getcwd()
    hydra_amcrest = cwd + "/data/hydra/Amcrest Camera/AmcrestCamBruteForce_1.pcap"

    fd = open(hydra_amcrest, 'rb')
    pcap = dpkt.pcap.Reader(fd)

    for timestamp, buf in pcap:
        # Unpack the Ethernet frame (mac src/dst, ethertype)
        eth = dpkt.ethernet.Ethernet(buf)
        
        # Make sure the Ethernet data contains an IP packet
        if not isinstance(eth.data, dpkt.ip.IP):
            print('Non IP Packet type not supported %s\n' % eth.data.__class__.__name__)
            continue
    
        ip = eth.data
        # Check for TCP in the transport layer
        if isinstance(ip.data, dpkt.tcp.TCP):

            # Set the TCP data
            tcp = ip.data

            try:
                request = dpkt.http.Request(tcp.data)
            except (dpkt.dpkt.NeedData, dpkt.dpkt.UnpackError):
                continue

            # Pull out fragment information (flags and offset all packed into off field, so use bitmasks)
            do_not_fragment = bool(ip.off & dpkt.ip.IP_DF)
            more_fragments = bool(ip.off & dpkt.ip.IP_MF)
            fragment_offset = ip.off & dpkt.ip.IP_OFFMASK

            # Print out the info
            #print('Timestamp: ', str(datetime.datetime.utcfromtimestamp(timestamp)))
            #print('Ethernet Frame: ', mac_addr(eth.src), mac_addr(eth.dst), eth.type)
            #print('IP: %s -> %s   (len=%d ttl=%d DF=%d MF=%d offset=%d)' % (ip_src, ip_dst, ip.len, ip.ttl, do_not_fragment, more_fragments, fragment_offset))
            print('HTTP request: %s\n' % repr(request))

test()
