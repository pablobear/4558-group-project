import pyshark
import os
import string
import asyncio

def get_cap(filename):
    cap = pyshark.FileCapture(filename)
    return cap

def get_files():
    cwd = os.getcwd()
    hydra_amcrest = cwd + "/data/hydra/Amcrest Camera/AmcrestCamBruteForce_"
    hydra_simcam = cwd + "/data/hydra/SimCam/SimcamBruteForce_"
    nmap_amcrest = cwd + "/data/nmap/Amcrest Camera/AmcrestCamRTSP_"
    nmap_simcam = cwd + "/data/nmap/SimCam/SIMCAMRTSP_"

    data_dict = {hydra_amcrest:3, hydra_simcam:3, nmap_amcrest: 3, nmap_simcam: 2}
    for pcap, number in data_dict.items():
        filenum = 1
        for x in range(number):
            print(pcap + str(filenum) + ".pcap")
            filename = pcap + str(filenum) + ".pcap"
            cap = get_cap(filename)
            cap.load_packets()
            packet_amount = len(cap)
            print(packet_amount)
            filenum += 1

def get_len(cap):
    packet_count = 0
    for x in cap:
        packet_count += 1
    print("Packets in cap:", packet_count)
    cap.close()

def trythis():
    get_files()
trythis()


