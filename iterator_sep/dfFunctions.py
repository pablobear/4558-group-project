import pandas as pd

def append(df, data):
    df.loc[len(df.index)] = data
    
def create_df():
    df = pd.DataFrame(columns = ['PCAP Name', 'IOT', "Attack Features", 'Exploit Tool', '# of Packets', '# of ARP Packets', 'Unique IPs',\
        'total_bits', 'average_traffic_rate', 'total_ipv4_packets', 'unique_ips', 'unique_src_ports',\
            'unique_dst_ports'])
    return df


def plot_df(df):
    ax = df.plot(x="Attack Features", y="# of Packets", kind="bar", figsize=(5,3))
    fig = ax.get_figure()
    fig.savefig("output/packetnum.pdf")
    
    ax = df.plot(x="Attack Features", y="Unique IPs", kind="bar", figsize=(5,3))
    fig = ax.get_figure()
    fig.savefig("output/uniqueips.pdf")
    
    ax = df.plot(x="Attack Features", y="total_bits", kind="bar", figsize=(5,3))
    fig = ax.get_figure()
    fig.savefig("output/totalbits.pdf")
    
    ax = df.plot(x="Attack Features", y="unique_src_ports", kind="bar", figsize=(5,3))
    fig = ax.get_figure()
    fig.savefig("output/unique_src_ports.pdf")
    
    ax = df.plot(x="Attack Features", y="unique_dst_ports", kind="bar", figsize=(5,3))
    fig = ax.get_figure()
    fig.savefig("output/unique_dst_ports.pdf")