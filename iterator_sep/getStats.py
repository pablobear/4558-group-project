import pandas as pd

def get_stats(data, filename):
    stats_df = pd.DataFrame(data, columns = ['timestamp', 'ip_size', 'src_ip', 'dst_ip', 'src_port', 'dst_port', 'protocol', 'version'])

    # Total Bits
    total_bits = stats_df['ip_size'].sum()

    # Average traffic rate
    first_ts = (stats_df.timestamp[0])

    last_ts = (stats_df.timestamp[len(stats_df.index)-1])

    average_traffic_rate = (total_bits*8)/(last_ts - first_ts)

    # Number of IPV4 packets
    total_ipv4_packets = sum(stats_df['version'] == 4)

    # Number of unique IPs
    unique_ips = len(stats_df.dst_ip.unique())
    # Number of unique Ports
    unique_src_ports = len(stats_df.src_port.unique())
    unique_dst_ports = len(stats_df.dst_port.unique())

#    print(filename)
#    print(stats_df.protocol.value_counts())
#    print(stats_df.src_port.value_counts())


    print(filename)
    #print(stats_df.src_port.value_counts())    
    
    return total_bits, average_traffic_rate, total_ipv4_packets, unique_ips, unique_src_ports, unique_dst_ports



