from getStats import get_stats
import socket
import dpkt 

def parse_pcap(filename):
    fd = open(filename, 'rb')
    pcap = dpkt.pcap.Reader(fd)
    packet = 0
    arp_packet_count = 0
    data = []

    tslist = []
    ipsrclist = []
    ipdstlist = []
    srcprtlist = []
    dstprtlist = []
    protolist = []
    verlist = []
    
    for ts, buf in pcap:
        packet += 1
        eth = dpkt.ethernet.Ethernet(buf)
        ip = eth.data
        
        
        try: 
            ip_src = socket.inet_ntoa(ip.src)
            ip_dst = socket.inet_ntoa(ip.dst)
            
            if ip.p == 6:
                ip_proto = 'TCP'
            elif ip.p == 17:
                ip_proto = 'UDP'
            elif ip.p == 1:
                ip_proto = 'ICMP'
            elif ip.p == 4:
                ip_proto = 'IP-in-IP'
            elif ip.p == 47:
                ip_proto = 'GRE'
            elif ip.p == 132:
                ip_proto = 'SCTP'
            else: ip_proto = ip.p

            try: 
                srcport = ip.data.sport
                dstport = ip.data.dport
            except:
                srcport = 0
                dstport = 0
            data.append([ts, len(ip), ip_src, ip_dst, srcport, dstport, ip_proto, ip.v])
        except:
            # This is an ARP packet
            arp_packet_count += 1
    print(data[0])
    total_bits, average_traffic_rate, total_ipv4_packets, \
    unique_ips, unique_src_ports, unique_dst_ports \
    = get_stats(data, filename)


    #print("Packets in pcap:", packet)
    #print("ARP packets in pcap:", arp_packet_count)
    return packet, arp_packet_count, total_bits, average_traffic_rate, \
        total_ipv4_packets, unique_ips, unique_src_ports, unique_dst_ports