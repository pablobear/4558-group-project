from dfFunctions import append
from dfFunctions import create_df
from dfFunctions import plot_df
from pcapParse import parse_pcap

import os

def main():
    # File Structure:
    #   Hydra :
    #       Amcrest - 3 pcaps 
    #           "AmcrestCamBruteForce_#.pca"
    #       SimCam - 3 pcaps
    #           "SimcamBruteForce_#.pcap"
    #   Nmap :   
    #       Amcrest - 3 pcaps
    #           "AmcrestCamRTSP_#.pcap"
    #       SimCam - 2 pcaps
    #           "SIMCAMRTSP_#.pcap"

    cwd = os.getcwd()
    cwd = "/home/paul.duhe/traffic_analysis/4558-group-project/"
    hydra_amcrest = cwd + "/data/hydra/Amcrest Camera/AmcrestCamBruteForce_"
    hydra_simcam = cwd + "/data/hydra/SimCam/SimcamBruteForce_"
    nmap_amcrest = cwd + "/data/nmap/Amcrest Camera/AmcrestCamRTSP_"
    nmap_simcam = cwd + "/data/nmap/SimCam/SIMCAMRTSP_"

    data_dict = {hydra_amcrest:3, hydra_simcam:3, nmap_amcrest: 3, nmap_simcam: 2}
    df = create_df()
    for filename, number in data_dict.items():
        filenum = 1
        for x in range(number):
            basename = os.path.basename(filename) + str(filenum) + ".pcap"
            #print("Parsing " + filename + str(filenum) + ".pcap")

            packet_count, arp_count, total_bits, average_traffic_rate, \
            total_ipv4_packets, unique_ips, unique_src_ports, unique_dst_ports \
                = parse_pcap(filename + str(filenum) + ".pcap")
        
            if ('hydra' in filename):
                exploit_tool = 'hydra'
            else:
                exploit_tool = 'nmap'
            
            if ('Amcrest' in filename):
                iot = 'Amcrest'
            else:
                iot = 'nmap'
                
            attack_features = iot, '+', exploit_tool

            new_row = [basename, iot, attack_features, exploit_tool, packet_count, arp_count, unique_ips, total_bits, average_traffic_rate, total_ipv4_packets, unique_ips, unique_src_ports, unique_dst_ports]
            append(df, new_row)
            filenum += 1

    #fd = open(hydra_amcrest,"rb")
    #print("opening", hydra_amcrest)
    #pcap = dpkt.pcap.Reader(fd)
    print(df)   
    df.to_csv(cwd + "iterator_sep/output/df_output.csv", index=False)
    plot_df(df) 
    
main()